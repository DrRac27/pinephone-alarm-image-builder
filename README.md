# Arch Linux ARM on PinePhone and PineTab
This script creates a rootfs that can be installed using the [FDE installer](https://github.com/dreemurrs-embedded/archarm-mobile-fde-installer) (if modified a bit).
The only tested rootfs right now is using Phosh on the PinePhone.

## Dependencies
- this script has to be run on Arch Linux ARM (e.g. in a qemu VM) and as root
- `pacstrap`, `arch-chroot` and `mksquashfs` (`pacman -S arch-install-scripts squashfs-tools`)

## Own customization
The file `misc/custom-setup.sh` runs inside of the image right at the end before it is being compressed.
If you want to use it you can start with the example file (e.g. using `cp misc/custom-setup.sh{.example,}`).
