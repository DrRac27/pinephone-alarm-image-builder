#!/bin/bash

set -e


# Settings
BUILD_DIR="$PWD/rootfs"
OUTPUT_DIR="$PWD/sqfs"
MISC_DIR="$PWD/misc"
PKGS="base bootsplash-theme-danctnix osk-sdl sudo networkmanager" # TODO: move networkmanager to phosh?


# Check if running in the right architecture
if [ "$(uname -m)" != "aarch64" ]; then
	echo 'This script has to be run on aarch64 architecture.' >&2
	echo 'For example on the PinePhone, some SBC or using qemu.' >&2
	exit 1
fi


# Check if running as root
if [ "$(id -u)" -ne "0" ]; then
	echo 'Please run as root.' >&2
	exit 1
fi


# Check dependencies
function check_dependency {
	dependency=$1
	hash $dependency >/dev/null 2>&1 || {
		echo >&2 "${dependency} not found. Please make sure it is installed and on your PATH."
		exit 1
	}
}
check_dependency "pacstrap"
check_dependency "arch-chroot"
check_dependency "mksquashfs"


# Create build dirs if they don't exist
mkdir -p "$BUILD_DIR"
mkdir -p "$OUTPUT_DIR"


# Ask for the device
echo -e "\e[1mWhich image do you want to create?\e[0m"
select OPTION in "PinePhone" "PineTab"; do
	case $OPTION in
		"PinePhone" )
			DEVICE="pinephone"
			PKGS="$PKGS device-pine64-pinephone"
			break;;
		"PineTab" )
			DEVICE="pinetab"
			PKGS="$PKGS device-pine64-pinetab"
			break;;
	esac
done


# Ask for the desktop environment
echo -e "\e[1mWhich environment would you like to install?\e[0m"
select OPTION in "Phosh" "Plasma" "Barebone"; do
	case $OPTION in
		"Phosh" )
			USR_ENV="phosh"
			PKGS="$PKGS danctnix-phosh-ui-meta dialog kgx"
			break;;
		"Plasma" )
			echo "WARNING: This is completely untested and will probably not work!"
			USR_ENV="plasma"
			PKGS="$PKGS danctnix-pm-ui-meta"
			break;;
		"Barebone" )
			echo "WARNING: This is completely untested and will probably not work!"
			USR_ENV="barebone"
			PKGS="$PKGS danctnix-usb-tethering openssh"
			break;;
	esac
done


# Ask for customization details
read -p "Login username: " USERNAME
[ "$USR_ENV" == "phosh" ] && echo "WARNING: Phosh only allows numbers as password. If you use letters you will not be able to log in!"
read -sp "Login password: " PASSWORD
echo "" # invisible password does not end with new line
read -p "Hostname: " HOSTNAME


# from arch-chroot man page:
# "If your chroot target is not a mountpoint, you can bind mount the directory on itself to make it a mountpoint"
mount --bind "$BUILD_DIR" "$BUILD_DIR"


# Install the base system
pacstrap -C "$MISC_DIR/pacman.conf.aarch64" "$BUILD_DIR" $PKGS


# basic configuration
sed 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/g' -i "$BUILD_DIR/etc/sudoers"
install -Dm 644 "$MISC_DIR/pacman.conf.aarch64" "$BUILD_DIR/etc/pacman.conf"
echo $HOSTNAME > "$BUILD_DIR/etc/hostname"
sed 's/HOOKS=(base udev autodetect modconf block filesystems keyboard fsck)/HOOKS=(base udev autodetect modconf block filesystems keyboard osk-sdl fsck)/' -i "$BUILD_DIR/etc/mkinitcpio.conf"
arch-chroot "$BUILD_DIR" mkinitcpio -P
arch-chroot "$BUILD_DIR" systemctl enable NetworkManager
arch-chroot "$BUILD_DIR" systemctl enable ModemManager


# create the default user and give a password
arch-chroot "$BUILD_DIR" useradd -m -G games,network,scanner,rfkill,video,storage,optical,lp,input,audio,wheel $USERNAME
echo "$USERNAME:$PASSWORD" | arch-chroot "$BUILD_DIR" chpasswd


# user environment specific stuff
if [ "$USR_ENV" == "phosh" ] ; then
	arch-chroot "$BUILD_DIR" systemctl enable phosh
elif [ "$USR_ENV" == "plasma" ] ; then
	arch-chroot "$BUILD_DIR" systemctl enable lightdm
elif [ "$USR_ENV" == "barebone" ] ; then
	arch-chroot "$BUILD_DIR" systemctl enable sshd
	arch-chroot "$BUILD_DIR" systemctl enable usb-tethering
else
	echo "This is a bug, please check the code. (\$USR_ENV=$USR_ENV not handled)"
	exit 1
fi


# Run the additional and optional customization script
if [ -f "$MISC_DIR/custom-setup.sh" ]; then
	install -Dm 755 "$MISC_DIR/custom-setup.sh" "$BUILD_DIR/bin/custom-setup"
	arch-chroot "$BUILD_DIR" custom-setup
	rm "$BUILD_DIR/bin/custom-setup"
fi


# create a squashfs out of the rootfs
SQFSNAME="archlinux-$DEVICE-$USR_ENV-$(date +%Y-%m-%d-%H%M%S).sqfs"
mksquashfs "$BUILD_DIR" "$OUTPUT_DIR/$SQFSNAME"
sha512sum "$OUTPUT_DIR/$SQFSNAME" > "$OUTPUT_DIR/$SQFSNAME.sha512sum"


# Rename to fit the name the FDE installer wants
cd $OUTPUT_DIR
ln -sf $SQFSNAME archlinux-$DEVICE-$USR_ENV.sqfs
sha512sum archlinux-$DEVICE-$USR_ENV.sqfs > archlinux-$DEVICE-$USR_ENV.sqfs.sha512sum


# clean up
umount "$BUILD_DIR"
rm -rf "$BUILD_DIR"


echo "Success!"
